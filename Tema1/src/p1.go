package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net"
	"strconv"
	"strings"
)

//permutare numar
func permutare(n int) int {
    new_int := 0
    for n > 0 {
        remainder := n % 10
        new_int *= 10
        new_int += remainder 
        n /= 10
    }
    return new_int 
}

//suma numerelor permutate
func suma(vector []int) int {
	var sum = 0
	for i := 0; i < len(vector); i++ {
		x := permutare(vector[i])
		sum = sum + x
	}
	return sum
}

//recreare vector
func newVector(vector []int) []int {
	var vectorNou []int
	for i := 0; i < len(vector); i++ {
		x := permutare(vector[i])
		vectorNou = append(vectorNou, x)

	}
	return vectorNou
}

func check(err error, message string) {
	if err != nil {
		panic(err)
	}

	fmt.Printf("%s\n", message)
}

func main() {

	//preluam data din config.txt
	data, err := ioutil.ReadFile("config.txt")
	if err != nil {
		fmt.Println("Cannot read from file ", err)
		return
	}
	lungime := string(data)
	lung, _ := strconv.Atoi(lungime)

	//cifre = cel mai mare nr de 'lung' cifre
	var cifre = 0
	for lung > 0 {
		cifre = cifre*10 + 9
		lung = lung - 1
	}
	//setam conexiunile
	clientCount := 0
	allClients := make(map[net.Conn]int)

	ln, err := net.Listen("tcp", ":8080")
	check(err, "Server is ready.")

	for {
		conn, err := ln.Accept()
		if err != nil {
			panic(err)
		}

		allClients[conn] = clientCount
		fmt.Printf("Clientul %d este conectat.\n", allClients[conn])

		clientCount++

		go func() {
			reader := bufio.NewReader(conn)
			//preluam datele de la client
			for {
			eticheta:
				incoming, err := reader.ReadString('\n')

				if err != nil {
					fmt.Printf("Client disconnected.\n")
					break
				}

				fmt.Printf("Client %d a facut request cu datele: %s", allClients[conn], incoming)
				conn.Write([]byte("Serverul a primit requestul.\n"))
				//stergem '/n' din mesaj
				incoming = incoming[0 : len(incoming)-2]

				//formam un vector de int si verificam nr
				v := strings.Split(incoming, ",")
				var vector []int
				for i := 0; i < len(v); i++ {
					elem, _ := strconv.Atoi(v[i])
					if elem > cifre {
						cif := strconv.Itoa(cifre)
						conn.Write([]byte("Eroare!Ati introdus un numar mai mare de " + cif + " elemente.\n"))
						goto eticheta
					}
					vector = append(vector, elem)
				}

				//determinam vectorul de nr permutate+suma lor
				conn.Write([]byte("Server proceseaza datele.\n"))
				vector2 := newVector(vector[:])
				s := suma(vector[:])
				sum := strconv.Itoa(s)

				//creeam vector de stringuri cu nr permutate
				vectorPermutat := ""
				for i := 0; i < len(vector2); i++ {
					elem2 := strconv.Itoa(vector2[i])
					vectorPermutat = vectorPermutat + elem2 + ","
				}
				//taiem "," din capatul stringului
				vectorPermutat = vectorPermutat[0 : len(vectorPermutat)-1]

				nume := strconv.Itoa(allClients[conn])

				fmt.Printf("Server trimite " + incoming + " => " + vectorPermutat + " cu suma " + sum + " catre client.\n")
				conn.Write([]byte("Client " + nume + " a primit raspunsul: " + vectorPermutat + " cu suma " + sum + "\n"))
			}
		}()
	}
}
